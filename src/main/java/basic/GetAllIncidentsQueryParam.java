package basic;

import java.util.HashMap;
import java.util.Map;

import io.restassured.RestAssured;
import io.restassured.response.Response;

import org.testng.annotations.Test;

public class GetAllIncidentsQueryParam {

	@Test
	public void getAllIncidents(){
	RestAssured.baseURI="https://dev53783.service-now.com/api/now/table/incident";
	RestAssured.authentication=RestAssured.basic("admin", "Z2xWNgcdCvI9");
	
	Map<String,String> param=new HashMap<String, String>();
	param.put("sysparm_fields","sys_id,number,category");
	param.put("category","software");
	
	Response response = RestAssured
			.given()
//			.queryParam("sysparm_fields","sys_id,number,category")
//			.queryParam("category","software")
			.queryParams(param)
			.get();
	System.out.println(response.getStatusCode());
	System.out.println(response.contentType());
	response.prettyPrint();
	}
}
