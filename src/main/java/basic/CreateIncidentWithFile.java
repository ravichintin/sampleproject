package basic;

import java.io.File;
import java.util.List;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;

import org.testng.annotations.Test;

public class CreateIncidentWithFile {
@Test
public void createIncidentWithOutBody(){
	RestAssured.baseURI="https://dev53783.service-now.com/api/now/table/incident";
	RestAssured.authentication=RestAssured.basic("admin", "Z2xWNgcdCvI9");
	
	File jsonFile=new File("CreateIncident.json");
	
	Response response=RestAssured
			.given()
			.contentType(ContentType.JSON)
			.when()
			.body(jsonFile)
			.accept(ContentType.XML)
			.post();
	
	System.out.println(response.getStatusCode());
	System.out.println(response.contentType());
	response.prettyPrint();
	
	//conver the response to xml
	XmlPath xmlPath = response.xmlPath();
	
	//get Sys_id
	List<String> listshort_description = xmlPath.getList("response.result.short_description");
	System.out.println(listshort_description);
	
}
}
