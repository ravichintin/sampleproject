package chaining;

import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

public class BaseRequest {
	public static RequestSpecification request;
	public static String sys_id; 
	@BeforeSuite
	public void initial(){
		
		RestAssured.baseURI= "https://dev53783.service-now.com/api/now/table/incident";
		RestAssured.authentication=RestAssured.basic("admin", "Z2xWNgcdCvI9");
		request = RestAssured
				.given()
				.log()
				.all()
				.contentType(ContentType.JSON);
	}
}
